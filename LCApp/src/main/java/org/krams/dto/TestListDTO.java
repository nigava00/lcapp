package org.krams.dto;

import java.util.List;

import org.krams.domain.Test;

public class TestListDTO {
	
	private List<Test> tests;

	public List<Test> getTests() {
		return tests;
	}

	public void setTests(List<Test> tests) {
		this.tests = tests;
	}


}
