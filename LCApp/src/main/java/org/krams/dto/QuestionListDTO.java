package org.krams.dto;

import java.util.List;

import org.krams.domain.Question;

public class QuestionListDTO {

	
	private List<Question> questions;

	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}
}
