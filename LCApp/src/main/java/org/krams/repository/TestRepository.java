package org.krams.repository;

import org.krams.domain.Test;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TestRepository extends MongoRepository<Test, String> {
	
	Test findByName(String name);

}
