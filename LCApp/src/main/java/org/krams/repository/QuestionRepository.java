package org.krams.repository;

import java.util.List;

import org.krams.domain.Question;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface QuestionRepository extends MongoRepository<Question, String> {

	Question findQuestionById(String id);
	
	List<Question> findByTopicLikeAndSubtopicsLikeAndTagsLike(String topic,String subtopics,String tags);

}
