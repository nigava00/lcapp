package org.krams.controller;

import org.krams.domain.Test;
import org.krams.dto.TestListDTO;
import org.krams.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping("/tests")
public class TestController {

	@Autowired
	private TestService service;
	
	@RequestMapping
	public String getTestsPage() {
		return "tests";
	}
	
	@RequestMapping(value="/records")
	public @ResponseBody TestListDTO getTests() {
		
		TestListDTO testListDto = new TestListDTO();
		testListDto.setTests(service.readAll());
		return testListDto;
	}
	
	@RequestMapping(value="/get")
	public @ResponseBody Test get(@RequestBody Test test) {
		return service.read(test);
	}

	
}
