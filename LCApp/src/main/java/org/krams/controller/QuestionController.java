package org.krams.controller;

import org.krams.domain.Question;
import org.krams.dto.QuestionListDTO;
import org.krams.service.QuestionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/questions")
public class QuestionController {
	
	@Autowired
	private QuestionsService service;
	
	@RequestMapping
	public String getQuestionPage() {
		return "QuestionsAdmin";
	}
	
	@RequestMapping(value="/records")
	public @ResponseBody QuestionListDTO getQuestions() {
		
		QuestionListDTO questionListDto = new QuestionListDTO();
		questionListDto.setQuestions(service.readAll());
		return questionListDto;
	}
	
	@RequestMapping(value="/get")
	public @ResponseBody Question get(@RequestBody Question question) {
		return service.read(question);
	}
	
	
//	public @ResponseBody QuestionListDTO findByKey(@RequestParam String topic,@RequestParam String subtopic){
//		QuestionListDTO questionListDto = new QuestionListDTO();
//		questionListDto.setQuestions(service.findByKeyWords(topic));
//		return questionListDto;
//	}
	@RequestMapping(value="/recordsSearch")
	public @ResponseBody QuestionListDTO getFilteredQuestions(@RequestParam String topic,
			@RequestParam String subtopic,
			@RequestParam String tag){
		
		QuestionListDTO questionListDto = new QuestionListDTO();
		questionListDto.setQuestions(service.findByTopicLikeAndSubtopicsLikeAndTagsLike(topic,subtopic,tag));
		return questionListDto;
	}


}
