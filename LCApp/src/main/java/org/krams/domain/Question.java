package org.krams.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Question {

	@Id
	private String id;
	
	private String questionText;
	
	private ArrayList<String> topic;
	
	private int questionType;
	
	private List<String> options;
	
	private String author;
	
	private Date creationDate;
	
	private List<String> answers;
	
	
	private List<ExamYear> examYear;
	
	private List<String> subtopics;
	
	public List<String> getSubtopics() {
		return subtopics;
	}

	public void setSubtopics(List<String> subtopics) {
		this.subtopics = subtopics;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	private List<String> tags;
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getQuestionText() {
		return questionText;
	}

	public void setQuestionText(String questionText) {
		this.questionText = questionText;
	}

	public ArrayList<String> getTopic() {
		return topic;
	}

	public void setTopic(ArrayList<String> topic) {
		this.topic = topic;
	}

	public int getQuestionType() {
		return questionType;
	}

	public void setQuestionType(int questionType) {
		this.questionType = questionType;
	}

	

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}


	public List<ExamYear> getExamYear() {
		return examYear;
	}

	public void setExamYear(List<ExamYear> examYear) {
		this.examYear = examYear;
	}
	
	public List<String> getOptions() {
		return options;
	}

	public void setOptions(List<String> options) {
		this.options = options;
	}

	public List<String> getAnswers() {
		return answers;
	}

	public void setAnswers(List<String> answers) {
		this.answers = answers;
	}


	
	
}
