package org.krams.domain;

public class ExamYear {

	private int year;
	
	private String exam;
	
	public ExamYear(int year, String exam){
		this.year = year;
		this.exam = exam;
	}
	
	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getExam() {
		return exam;
	}

	public void setExam(String exam) {
		this.exam = exam;
	}

	
	
}
