package org.krams.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.krams.domain.ExamYear;
import org.krams.domain.Question;
import org.krams.domain.Role;
import org.krams.domain.Test;
import org.krams.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

/**
 * Service for initializing MongoDB with sample data using {@link MongoTemplate}
 */
public class InitMongoService {
	
	@Autowired
	private MongoTemplate mongoTemplate;

	public void init() {
		// Drop existing collections
		mongoTemplate.dropCollection("role");
		mongoTemplate.dropCollection("user");
        mongoTemplate.dropCollection("test");
        mongoTemplate.dropCollection("question");
        
        
        List<Role> rolelist = new ArrayList<Role>();
		// Create new records
		Role adminRole = new Role();
		adminRole.setId(UUID.randomUUID().toString());
		adminRole.setRole(1);
		
		rolelist.add(adminRole);
		
		Role userRole = new Role();
		userRole.setId(UUID.randomUUID().toString());
		userRole.setRole(2);
		
		rolelist.add(userRole);
		
		List<User> userlist = new ArrayList<User>();
		
		User john = new User();
		john.setId(UUID.randomUUID().toString());
		john.setFirstName("John");
		john.setLastName("Smith");
		john.setPassword("21232f297a57a5a743894a0e4a801fc3");
		john.setRole(adminRole);
		john.setUsername("john");
		
		userlist.add(john);
		
		User jane = new User();
		jane.setId(UUID.randomUUID().toString());
		jane.setFirstName("Jane");
		jane.setLastName("Adams");
		jane.setPassword("ee11cbb19052e40b07aac0ca060c23ee");
		jane.setRole(userRole);
		jane.setUsername("jane");
		
		userlist.add(jane);
		
		
		List<Test> testlist = new ArrayList<Test>();
		
		Test phy = new Test();
		phy.setId(UUID.randomUUID().toString());
		phy.setAuthor("Admin");
		phy.setCategory("Physics");
		phy.setCreationDate((new Date()));
		phy.setName("Phyics Basic Quesions");
		phy.setTotalQuestions(30);
		phy.setNoAttempts(0);
		
		testlist.add(phy);
		
		Test chem = new Test();
		chem.setId(UUID.randomUUID().toString());
		chem.setAuthor("Admin");
		chem.setCategory("Chemistry");
		chem.setCreationDate((new Date()));
		chem.setName("Chemistry Basic Quesions");
		chem.setTotalQuestions(30);
		chem.setNoAttempts(0);
		
		testlist.add(chem);
		
		Test math = new Test();
		math.setId(UUID.randomUUID().toString());
		math.setAuthor("Admin");
		math.setCategory("Math");
		math.setCreationDate((new Date()));
		math.setName("Math Basic Quesions");
		math.setTotalQuestions(30);
		math.setNoAttempts(0);
		
		testlist.add(math);
		
		
		List<Question> questionlist = new ArrayList<Question>();
		
		Question q1 = new Question();
		q1.setId(UUID.randomUUID().toString());
		String ques = "1. public class Test {  <br/>"+
                      "2. public static void main(String args[]) { <br/>"+
                      "3. class Foo {  <br/>"+
                      "4. 	public int i = 3;  <br/>"+
                      "5. 	} <br/>"+
                      "6. 	Object o = (Object)new Foo();  <br/>"+
                      "7. 	Foo foo = (Foo)o;  <br/>"+
                      "8.	 System.out.println( &quot;i =  &quot; + foo.i); <br/>"+
                      "9.  	} <br/>"+
                      "10. }";
		
		q1.setQuestionText(ques);
		List<String> listans = new ArrayList<String>();
		
		listans.add("A#@i = 3");
		
		q1.setAnswers(listans);
		
		
		List<String> options1 = new ArrayList<String>();
		options1.add("A#@i = 3");
		options1.add("B#@Compilation fails.");
		options1.add("C#@A ClassCastException is thrown at line 6.");
		options1.add("D#@A ClassCastException is thrown at line 7.");
		q1.setOptions(options1);
		
		q1.setQuestionType(1);
		q1.setAuthor("Admin");
		q1.setCreationDate(new Date());
		
		List<ExamYear> listexamyr = new ArrayList<ExamYear>();
		ExamYear examYear1 = new ExamYear(2005,"SCJP2");
		listexamyr.add(examYear1);
		ExamYear examYear2 = new ExamYear(2005,"SCJP2");
		listexamyr.add(examYear2);
		ExamYear examYear3 = new ExamYear(2005,"SCJP2");
		listexamyr.add(examYear3);
		
		q1.setExamYear(listexamyr);
		
		ArrayList<String> topics = new ArrayList<String>();
		topics.add("Java");
		q1.setTopic(topics);
		
		ArrayList<String> subtopics = new ArrayList<String>();
		subtopics.add("JAVA Object Oriented");
		
		q1.setSubtopics(subtopics);
		
		ArrayList<String> listtags = new ArrayList<String>();
		listtags.add("Object Oriented");
		listtags.add("Inner Classes");
		
		q1.setTags(listtags);

		questionlist.add(q1);
		
		Question q2 = new Question();
		q2.setId(UUID.randomUUID().toString());
		String ques2 = "11. int i =1,j =10;  <br/>"+
                       "12. do { <br/>"+
                       "13. if(i++> --j) {<br/>"+
                       "14. continue; <br/>"+
                       "15. } <br/>"+
                       "16. } while (i <5); <br/>"+
                       "17. System.out.println(&quot;i = &quot; +i+ &quot;and j = &quot;+j);";
		
		q2.setQuestionText(ques2);
		
    
		List<String> listans1 = new ArrayList<String>();
		listans1.add("D#@i = 5 and j = 6");
		q2.setAnswers(listans1);
		
		List<String> options2 = new ArrayList<String>();
		options2.add("A#@i = 6 and j = 5");
		options2.add("B#@i = 5 and j = 5");
		options2.add("C#@i = 6 and j = 5");
		options2.add("D#@i = 5 and j = 6");
		options2.add("E#@i = 6 and j = 6");
		
		List<String> listtags2 = new ArrayList<String>();
		listtags2.add("SCJP");
		listtags2.add("JAVA");
		listtags2.add("JAVA FUNDAMENTALS");
		listtags2.add("JAVA Loops and Controls");
		
		q2.setTags(listtags2);

		q2.setOptions(options2);
		
		List<String> subtopic2 = new ArrayList<String>();
		subtopic2.add("JAVA BASIC");
		subtopic2.add("Loops and Controls");
		
		q2.setSubtopics(subtopic2);
		
		q2.setQuestionType(1);
		q2.setAuthor("Admin");
		q2.setCreationDate(new Date());
		q2.setExamYear(listexamyr);
		ArrayList<String> topics2 = new ArrayList<String>();
		topics2.add("Java 5");
		q2.setTopic(topics2);
		
		
		questionlist.add(q2);
		
		// Insert to db
		mongoTemplate.insert(userlist, "user");
		mongoTemplate.insert(rolelist, "role");
		mongoTemplate.insert(testlist, "test");
		mongoTemplate.insert(questionlist, "question");
	}
}
