package org.krams.service;

import java.util.List;
import java.util.UUID;

import org.krams.domain.Test;
import org.krams.repository.TestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TestService {

	@Autowired
	private TestRepository testRepository;
	
	
	public Test create(Test user) {
		user.setId(UUID.randomUUID().toString());
		
		return testRepository.save(user);
	}
	
	public Test read(Test test) {
		return test;
	}
	
	public List<Test> readAll() {
		return testRepository.findAll();
	}
	
	public Test update(Test test) {
		Test existingTest = testRepository.findByName(test.getName());
		
		if (existingTest == null) {
			return null;
		}
		
		existingTest.setName(test.getName());
		existingTest.setAuthor(test.getAuthor());
		

		return testRepository.save(existingTest);
	}
	
	public Boolean delete(Test test) {
		Test existingTest = testRepository.findByName(test.getName());
		
		if (existingTest == null) {
			return false;
		}
		testRepository.delete(existingTest);
		return true;
	}
	
	
	
	
}
