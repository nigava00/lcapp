package org.krams.service;

import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;

import org.krams.domain.Question;
import org.krams.repository.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

@Service
public class QuestionsService {


	@Autowired
	QuestionRepository questionsRepository;
	
	@Autowired
	MongoTemplate mongoTemplate;
	
	
	public Question create(Question question) {
		question.setId(UUID.randomUUID().toString());
		
		return questionsRepository.save(question);
	}

	
	public Question read(Question question) {
		return question;
	}
	
	public List<Question> readAll() {
		//return questionsRepository.findAll();
		
		return questionsRepository.findAll();
	}
	
	public Question update(Question question) {
		Question existingQuestion = questionsRepository.findQuestionById(question.getId());
		
		if (existingQuestion == null) {
			return null;
		}
		
		existingQuestion.setQuestionText(question.getQuestionText());
		existingQuestion.setAnswers(question.getAnswers());
		existingQuestion.setExamYear(question.getExamYear());
		existingQuestion.setOptions(question.getOptions());
		existingQuestion.setQuestionType(question.getQuestionType());
		existingQuestion.setTopic(question.getTopic());
		
		return questionsRepository.save(existingQuestion);
	}
	
	public boolean delete(Question question){
		Question existingQuestion = questionsRepository.findQuestionById(question.getId());
		
		if (existingQuestion == null) {
			return false;
		}
		
		questionsRepository.delete(question);
		return true;
	}
	
	public List<Question> findByTopicLikeAndSubtopicsLikeAndTagsLike(String topic,String subtopics,String tags){
		
		return questionsRepository.findByTopicLikeAndSubtopicsLikeAndTagsLike(topic,subtopics,tags);
		
	}
	
	
	public List<Question> findByKeyWords(String key){
		Pattern pat = Pattern.compile(key,Pattern.CASE_INSENSITIVE);
		System.out.println(pat.toString());
		Query query = new Query(Criteria.where("topic").regex(pat));
        // Execute the query and find one matching entry
        return mongoTemplate.find(query, Question.class);
	
	}

}
