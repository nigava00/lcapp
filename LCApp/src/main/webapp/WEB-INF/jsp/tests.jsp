<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>

<c:url value="/tests/records" var="recordsUrl" />

<html>
<head>
<link rel='stylesheet' type='text/css' media='screen'
	href='<c:url value="/resources/css/style.css"/>' />
<script type='text/javascript'
	src='<c:url value="/resources/js/jquery-1.6.4.min.js"/>'></script>
<script type='text/javascript'
	src='<c:url value="/resources/js/custom.js"/>'></script>

<title>User Records</title>

<script type='text/javascript'>
	$(function() {
		// init
		urlHolder.records = '${recordsUrl}';
		loadTable();
		
	});
	
	
	function loadTable() {
		$.get(urlHolder.records, function(response) {
			
	 		$('#tableTests').find('tbody').remove();
	 		
	 		for (var i=0; i<response.tests.length; i++) {
				var row = '<tr>';
				row += '<td><input type="radio" name="index" id="index" value="'+i+'"></td>';
				row += '<td>' + response.tests[i].name + '</td>';
				row += '<td>' + response.tests[i].category + '</td>';
				row += '<td>' + response.tests[i].totalQuestions + '</td>';
				row += '<td>' + response.tests[i].noAttempts+ '</td>';
				row += '</tr>';
		 		$('#tableTests').append(row);
	 		}
	 		
	 		$('#tableTests').data('model', response.tests);
	 	});
	}
	
	</script>
</head>

<body>
	<h1 id='banner'>Available Test Category</h1>
	<hr />

	<table id='tableTests'>
		<caption></caption>
		<thead>
			<tr>
				<th></th>
				<th>TestName</th>
				<th>Category</th>
				<th>No of Questions</th>
				<th>No of Attempts</th>
			</tr>
		</thead>
	</table>


</body>
</html>