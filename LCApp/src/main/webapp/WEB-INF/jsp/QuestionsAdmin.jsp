<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>

<c:url value="/questions/records" var="recordsUrl" />
<c:url value="/questions/recordsSearch" var="recordsSearchUrl" />

<html>
<head>
<link rel='stylesheet' type='text/css' media='screen'
	href='<c:url value="/resources/css/style.css"/>' />
<script type='text/javascript'
	src='<c:url value="/resources/js/jquery-1.6.4.min.js"/>'></script>
<script type='text/javascript'
	src='<c:url value="/resources/js/custom.js"/>'></script>

<title>Questions</title>

<script type='text/javascript'>
	$(function() {
		// init
		urlHolder.records = '${recordsUrl}';
		urlHolder.recordsSearch = '${recordsSearchUrl}';
		loadTable();
		
	});
	
	function formatAnswersAndOptions(b){
		var j=1;
		var answers="";
		jQuery.each(b, function(index, value) {
		   		 var ans=this.split("#@");
		   		 answers+=(j+index)+")"+ans[0]+":"+ans[1]+"<br/>";
		});
		
	 return answers;
	}
	
	
	function formatExamYear(examYear){
		var list = "";
		var j = 1;
		jQuery.each(examYear,function(index,value){
			var exyr = "";
			jQuery.each(value,function(i,ey){
				
				exyr+=" "+i.toUpperCase()+":"+ey;
			});
			list+="("+exyr+")"+"<br/>";
		});
		return list;
	}
	
	
	function searchQuestion(){
		
		$.post(urlHolder.recordsSearch, {
			topic: $('#topic').val(),
			subtopic:$('#subtopic').val(),
			tag:$('#tags').val()
		}, function(response) {
			populateTable(response);
		}
	);}
	
	
	function loadTable() {
		$.get(urlHolder.records,function(response){
			 populateTable(response);
		} );
	}
	
	  function populateTable(response){
			
	 		$('#tableQuestions').find('tbody').remove();
	 		
	 		for (var i=0; i<response.questions.length; i++) {
				var row = '<tr>';
				row += '<td><input type="radio" name="index" id="index" value="'+i+'"></td>';
				row += '<td width="100%" nowrap="nowrap">' + response.questions[i].questionText + '</td>';
				row += '<td>' + response.questions[i].topic + '</td>';
				row += '<td>' +formatExamYear(response.questions[i].examYear) + '</td>';
				row += '<td>' + formatAnswersAndOptions(response.questions[i].answers) + '</td>';
				row += '<td width="100%" nowrap="nowrap">' + formatAnswersAndOptions(response.questions[i].options) + '</td>';
				row += '</tr>';
		 		$('#tableQuestions').append(row);
	 		}
	 		
	 		$('#tableQuestions').data('model', response.questions);
	 	}
	  
	function enterSearch(event){
		    if(event.keyCode == 13){
		        $("#btnSearch").click();
		    }
		}

	
	</script>
</head>

<body>

	<table id="searchbox">
		<tr class="widht:100%">
			<td><label>Topic</label>:<input type="text"
				onkeyup="enterSearch(event)" id="topic" maxlength="20" /></td>
			<td><label>SubTopics</label>:<input id="subtopic"
				onkeyup="enterSearch(event)" type="text" maxlength="20" /></td>
			<td><label>Tag</label>:<input id="tags"
				onkeyup="enterSearch(event)" type="text" maxlength="20" /></td>
			<td><button id="btnSearch" onclick="searchQuestion()">Search</button>
		</tr>
	</table>

	<h1 id='banner'>Available Questions</h1>

	<table id='tableQuestions'>
		<caption></caption>
		<thead>
			<tr class="widht:40%">
				<th></th>
				<th>Question</th>
				<th>Topic</th>
				<th>Exam & Year</th>
				<th>Answers</th>
				<th>Options</th>
			</tr>
			<tr>
				<td width="100%" nowrap="nowrap"></td>
			</tr>
		</thead>
	</table>


</body>
</html>